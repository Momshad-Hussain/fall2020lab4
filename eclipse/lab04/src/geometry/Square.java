package geometry;

public class Square extends Rectangle{

	public Square(double sideLenght) {
		super(sideLenght, sideLenght);
		
	}
	
	public String toString() {
		return "Area of Sqaure : " + this.getArea() + " , " + " Perimeter of Square : " + this.getPerimeter();
	}
}
