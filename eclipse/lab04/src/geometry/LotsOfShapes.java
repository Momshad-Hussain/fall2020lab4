package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shape = new Shape[5];
		shape[0] = new Circle (6.26);
		shape[1] = new Circle (4.23);
		shape[2] = new Rectangle (23.0,11.0);
		shape[3] = new Rectangle (2.0,35.0);
		shape[4] = new Square (7.0);
		
		for(int i = 0; i<shape.length ; i++) {
			System.out.println(shape[i]);
		}

	}

}
