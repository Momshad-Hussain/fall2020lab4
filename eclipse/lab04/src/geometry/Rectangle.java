package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	public double getLength() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	@Override
	public double getArea() {
		return (this.length)*(this.width);
	}
	@Override
	public double getPerimeter() {
		return (2*(this.length))+(2*(this.width));
	}
	
	public String toString() {
		return "Area of Rectangle : " + this.getArea() + " , " + " Perimeter of Rectangle : " + this.getPerimeter();
	}
}
