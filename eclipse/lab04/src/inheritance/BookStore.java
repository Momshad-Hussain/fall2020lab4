package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] book= new Book[5];
		
		book[0] = new Book("Harry Potter", "J.K Rollings");
		book[1] = new ElectronicBook("Green Signals", "Jairam Ramesh", 11);
		book[2] = new Book("Cash cow", "�lise Desaulniers");
		book[3] = new ElectronicBook("Ulysses", "James Joyce", 35);
		book[4] = new ElectronicBook("Jane Austen", "Pride and Prejudice", 23);
		
		for(int i = 0; i< book.length; i++) {
				System.out.println(book[i]);
		}
	}

}
