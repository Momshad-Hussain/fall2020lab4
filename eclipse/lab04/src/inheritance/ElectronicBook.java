package inheritance;

public class ElectronicBook extends Book {

    private int numberBytes;
    

	public ElectronicBook(String titleOfBook, String author, int bytes) {
		super(titleOfBook, author);
		numberBytes = bytes;
	}
	
	public String toString(){
		String fromBase = super.toString();
		return  fromBase+" Number Of Bytes : " +this.numberBytes;
}

	
}