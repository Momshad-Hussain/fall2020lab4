//Momshad Hussain 1837713
package inheritance;

public class Book {
	private  String titleOfBook;
	private  String author;

	public Book(String titleOfBook, String author) {
		this.titleOfBook = titleOfBook;
	 	this.author = author;
	}

public  String getTitle(){
	return this.titleOfBook;
}

public  String getAuthor(){
	return this.author;
}


public String toString(){
	return "Title Of Book : " + this.titleOfBook + ", " + "Author Name: " + 
		    this.author; 
}

}